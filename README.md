# gitee-monaco-editor

## 介绍

预构建的 monaco-editor 库，包含一些已构建好的资源，无需使用 monaco-editor-webpack-plugin 插件重新构建。

## 安装

```
yarn add git://gitee.com/gitee-frontend/gitee-monaco-editor.git#v2.0.0
```

## 版本说明

| 版本范围                   | 使用仓库          | 区别                                           |
|------------------------|---------------|----------------------------------------------|
| `>=v1.0.0 and <v2.0.0` | gitee 旧版      | webpack `output.publicPath` 配置为 `/webpacks/` |
| `v2.0.0`               | gitee-ent-web | webpack `output.publicPath` 配置为 `/vendor/monaco-editor` |

## 使用

引入样式文件和 js 模块：

```js
import 'gitee-monaco-editor/lib/index.css'
import monaco from 'gitee-monaco-editor'

const editor = monaco.editor.create(document.getElementById('editor'))
```

修改 webpack 配置，添加 CopyWebpackPlugin 插件配置：

```js
new CopyWebpackPlugin([
  {
    from: path.resolve(rootPath, 'node_modules/gitee-monaco-editor/lib'),
    to: outputPath,
    ignore: ['index.*']
  }
])
```

## 发布

在命令行中运行：

``` bash
# 创建 beta 预发行版
yarn release-beta

# 创建正式发行版
yarn release
```

## 许可

[MIT](LICENSE)
