## [2.0.1](https://gitee.com/gitee-frontend/gitee-monaco-editor/compare/v2.0.0...v2.0.1) (2021-07-29)


### Bug Fixes

* 补充最新的构建产物 ([731ac21](https://gitee.com/gitee-frontend/gitee-monaco-editor/commits/731ac21ff8883a09ff18ac8603ef3df8b75cd006))



# [2.0.0](https://gitee.com/gitee-frontend/gitee-monaco-editor/compare/v1.0.0...v2.0.0) (2021-07-29)


### Features

* 将 webpack ouput.publicPath 配置更改为 /vendor/monaco-editor (!2) ([a62fbf7](https://gitee.com/gitee-frontend/gitee-monaco-editor/commits/a62fbf7181d7621d549e365ae92b833d2cc101ce))


### BREAKING CHANGES

* 将 webpack ouput.publicPath 配置从 /webpacks/ 调整为 /vendor/monaco-editor



# 1.0.0 (2020-01-17)



